package sample.week3.RemoteControl;

// concrete command
public class VolumeDownCommand implements Command {

    private Device device;
    // (a)

    public VolumeDownCommand (Device device) {
        // (a)
        this.device = device;
    }

    @Override
    public void execute() {
        // (a)
        this.device.volumeDown();
    }
}