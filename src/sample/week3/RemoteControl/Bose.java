package sample.week3.RemoteControl;

// a comand receiver
public class Bose implements Device {

    private int volume = 0;
    private static final String name = "Bose";
    @Override
    public void on() {
        System.out.println(name + " is on");
    }

    @Override
    public void off() {
        System.out.println(name + " is off");
    }

    @Override
    public void volumeUp() {
        System.out.println(name + " is at volume " + ++volume);
    }

    @Override
    public void volumeDown() {
        System.out.println(name + " is at volume " + --volume);
    }

    // (b)
}