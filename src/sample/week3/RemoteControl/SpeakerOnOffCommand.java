package sample.week3.RemoteControl;

// concrete command
public class SpeakerOnOffCommand implements Command {

    private State state;
    private Device device;

    enum State {ON, OFF};


    // (a)
    //receiver
    public SpeakerOnOffCommand(Device device) {
        this.device = device;
        this.state = State.OFF;
    }

    @Override
    public void execute() {
        // (a)
        if(this.state == State.OFF) {
            this.state = State.ON;
            this.device.on();
        }
        else {
            this.state = State.OFF;
            this.device.off();
        }
    }
}