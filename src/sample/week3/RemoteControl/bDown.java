package sample.week3.RemoteControl;

// invoker
public class bDown implements Button {
    private Command cmd;
    @Override
    public void setCommand(Command cmd) {
       this.cmd = cmd;
    }

    @Override
    public void pressButton() {
      cmd.execute();
    }
}