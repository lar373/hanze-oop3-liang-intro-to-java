package sample.week3.RemoteControl;

// concrete command
public class VolumeUpCommand implements Command {
    private Device device;
    // (a)

    public VolumeUpCommand (Device device) {
        // (a)
        this.device = device;
    }

    @Override
    public void execute() {
        // (a)
        device.volumeUp();
    }
}