package sample.week3.templatemethodpattern;

public class ConcreteMyAbstractList extends MyAbstractList {
    private Object[] list;
    ConcreteMyAbstractList(){
       this.list = new Object[20];
    }

    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
        if(list[index] == null) {
            this.list[index] = element;
        }else {
            for (int i = list.length - 1 ; i < index - 1; i--) {
                rangeCheckForAdd(i);
                list[i] = list[i-1];
            }
        }
    }

    @Override
    public void rangeCheckForAdd(int index) {
        if(index>list.length)
            throw new ArrayIndexOutOfBoundsException();
    }
}
