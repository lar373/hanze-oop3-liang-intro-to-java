package sample.week3.templatemethodpattern;

import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Collection;

public abstract class MyAbstractList extends AbstractList {

    @Override
    public Object get(int i) {
        return null;
    }

    @Override
    public Object set(int index, Object element) {
        return null;
    }

    @Override
    public abstract void add(int index, Object element);

    @Override
    public Object remove(int index) {
        return super.remove(index);
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        rangeCheckForAdd(index);
        boolean modified = false;
        for (Object e : c) {
            add(index++, e);
            modified = true;
        }
        return modified;
    }

    public abstract void rangeCheckForAdd(int index);

}
