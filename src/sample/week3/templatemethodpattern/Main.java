package sample.week3.templatemethodpattern;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ConcreteMyAbstractList list = new ConcreteMyAbstractList();
        ArrayList<Integer> insertArray = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
           insertArray.add(i);
        }
        System.out.println(list.addAll(0, insertArray));
    }
}
