package sample.week3.Iterator;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MyStack implements Iterable<String> {

    private ArrayList<String> list = new ArrayList<>();

    @Override
    public Iterator<String> iterator() {
        return new StackIterator(list);
    }

    @Override
    public void forEach(Consumer<? super String> action) {

    }

    @Override
    public Spliterator<String> spliterator() {
        return null;
    }

    void push(String element) {
        list.add(element);
    }
    String pop(int index){
       return list.remove(list.size()-1);
    }
    boolean isEmpty(){
        return list.isEmpty();
    }

}
