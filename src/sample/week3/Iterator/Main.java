package sample.week3.Iterator;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        MyStack stack = new MyStack();

        stack.push("kwik");
        stack.push("kwek");
        stack.push("kwak");

        // your code
        for(Iterator<String> it = stack.iterator(); it.hasNext();){
            System.out.println(it.next());
        }
       System.out.println("--------------------------------------");

        for(String item: stack){
            System.out.println(item);
        }

    }
}
