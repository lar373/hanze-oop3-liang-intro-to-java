package sample.week3.Iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class StackIterator implements Iterator {
    ArrayList<String> myList;
    int indexPointer = 0;

    StackIterator(ArrayList<String> myList){
       this.myList = myList;
    }

    @Override
    public boolean hasNext() {
        return this.myList.size() - 1 >= indexPointer;
    }

    @Override
    public Object next() {
        return myList.get(indexPointer++);
    }
}
