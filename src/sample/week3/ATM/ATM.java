package sample.week3.ATM;

// the context
public class ATM
{
    private ATMState state;
    private int amount = 100;
    private int pinCode;
    // define an initial state
    public ATM(int pinCode) {
        setState(new IdleState());
        this.pinCode = pinCode;
    }

    void setState(ATMState state) {
        this.state = state;

    }

    ATMState getState() {
        return this.state;
    }

    void insertCard() {
        state.insertCard(this);
    }

    void ejectCard() {
        state.ejectCard(this);
    }

    void insertPin() {
        state.insertPin(this);
    }

    void requestAmount() {
        state.requestAmount(this);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
        System.out.println("amount set");
    }

    public int getPinCode() {
        return pinCode;
    }
}
