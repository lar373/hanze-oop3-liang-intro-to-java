package sample.week3.ATM;

public class OutOfService implements ATMState {
    @Override
    public void insertCard(ATM atm) {
        System.out.println("Out of Service!");
    }

    @Override
    public void ejectCard(ATM atm) {
        System.out.println("Out of Service!");
    }

    @Override
    public void insertPin(ATM atm) {
        System.out.println("Out of Service!");
    }

    @Override
    public void requestAmount(ATM atm) {
        System.out.println("Out of Service!");
    }
}
