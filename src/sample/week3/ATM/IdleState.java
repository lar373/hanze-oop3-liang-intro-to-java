package sample.week3.ATM;

public class IdleState implements ATMState {
    @Override
    public void insertCard(ATM atm) {
        System.out.println("inserting Card");
        atm.setState(new HasCard());
    }

    @Override
    public void ejectCard(ATM atm) {
        System.out.println("No card to be ejected");
    }

    @Override
    public void insertPin(ATM atm) {
        System.out.println("Insert card first");
    }

    @Override
    public void requestAmount(ATM atm) {
       System.out.println("Insert card first");
    }
}
