package sample.week3.ATM;

public class HasCorrectPin implements ATMState {
    private static final int amount = 20;
    @Override
    public void insertCard(ATM atm) {
        System.out.println("Card already inserted");
    }

    @Override
    public void ejectCard(ATM atm) {
        System.out.println("Ejecting card");
        atm.setState(new IdleState());
    }

    @Override
    public void insertPin(ATM atm) {
        System.out.println("Correct pin already inserteda");
    }

    @Override
    public void requestAmount(ATM atm) {
        System.out.println("requesting amount");
        if (atm.getAmount() > amount){
//            atm.setAmount(atm.getAmount()- amount);
            System.out.println("Enough money!");
            atm.setState(new HasAmount());}
        else{
            System.out.println("Not enough money!");
        }
    }
}
