package sample.week3.ATM;

import java.util.Scanner;

public class HasCard implements ATMState {
    @Override
    public void insertCard(ATM atm) {
        System.out.println("Card Already Inserted");
    }

    @Override
    public void ejectCard(ATM atm) {
        System.out.println("Ejecting Card");
        atm.setState(new IdleState());
    }

    @Override
    public void insertPin(ATM atm) {
        System.out.println("Insert pincode");
        Scanner sc = new Scanner(System.in);
        if(atm.getPinCode() == sc.nextInt()){
            System.out.println("Correct pin entered");
            atm.setState(new HasCorrectPin());
        }else{
            System.out.println("Invalid pincode");
        }
        //kan checken op pinwaarde en na 3x naar idle gaan
    }

    @Override
    public void requestAmount(ATM atm) {
       System.out.println("Insert pin first");
    }
}
