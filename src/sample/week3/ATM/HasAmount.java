package sample.week3.ATM;

public class HasAmount implements ATMState {
    @Override
    public void insertCard(ATM atm) {
        System.out.println("already inserted");
    }

    @Override
    public void ejectCard(ATM atm) {
        System.out.println("Ejecting Card");
        atm.setState(new IdleState());
    }

    @Override
    public void insertPin(ATM atm) {
        System.out.println("Correct pin already inserted");
    }

    @Override
    public void requestAmount(ATM atm) {
        System.out.println(atm.getAmount());
    }
}
