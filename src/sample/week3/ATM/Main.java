package sample.week3.ATM;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // create the context
        ATM atm = new ATM(1234);
        atm.setState(new IdleState());


            System.out.println("Type a number for an event:");
            System.out.println("1. Insert card");
            System.out.println("2. Enter PIN code");
            System.out.println("3. Enter amount");
            System.out.println("4. I want my money honey");
            System.out.println();


        // while loop
        while(!(atm.getState() instanceof OutOfService)) {
            System.out.println(atm.getState().toString());
            Scanner sc = new Scanner(System.in);
            ATMState atmState = atm.getState();
            int option = sc.nextInt();
            switch (option){
                case -1:
                    atm.setState(new OutOfService());
                    break;
                case 1:
                    atmState.insertCard(atm);
                    break;
                case 2:
                    atmState.insertPin(atm);
                    break;
                case 3:
                    System.out.println("Set amount to be retrieved");
                    atm.setAmount(sc.nextInt());
                    break;
                case 4:
                    atmState.requestAmount(atm);
                    break;
                default:
                    System.out.println("invalid number given try again");
            }
        }
    }
}