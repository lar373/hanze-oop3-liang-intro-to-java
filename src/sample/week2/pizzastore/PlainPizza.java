package sample.week2.pizzastore;

public class PlainPizza implements Pizza {

    private String size = "L";

    public String getDescription() {
        return "a plain pizza";
    }
    public double getCost() {
        if(this.size.equals("M")){
            System.out.println("Cost of medium plain pizza: " + 4.00);
            return 4.00;
        }else if(this.size.equals("L")){
            System.out.println("Cost of large plain pizza: " + 5.00);
            return 5.00;
        }else{
            System.out.println("Cost of small plain pizza: " + 3.00);
            return 3.00;
        }
    }

    @Override
    public String getSize() {
        return this.size;
    }

    @Override
    public void setSize(String size) {
        this.size = size;
    }
}
