package sample.week2.pizzastore;

public class TomatoSauce extends ToppingDecorator {

    public TomatoSauce(Pizza newPizza) {
        super(newPizza);
        System.out.println("Adding sauce");
    }
    
    public String getDescription() {
        return tempPizza.getDescription() + " + tomato sauce";
    }

    @Override
    public String getSize() {
        return super.getSize();
    }

    @Override
    public void setSize(String size) {
       //
    }

    public double getCost() {
        //if else with getsize
        System.out.println("Cost of sauce: " + .35);
        return tempPizza.getCost() + .35;
    }
}
