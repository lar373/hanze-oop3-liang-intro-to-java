package sample.week2.pizzastore;

public class Mozzarella extends ToppingDecorator {

    public Mozzarella(Pizza newPizza) {
        super(newPizza);
        System.out.println("Adding mozzarella");
    }
    
    public String getDescription() {
        return tempPizza.getDescription() + " + mozzarella";
    }

    @Override
    public String getSize() {
        return super.getSize();
    }

    @Override
    public void setSize(String size) {
       //
    }

    public double getCost() {
       //if else with getSize
        System.out.println("Cost of mozzarella: " + .50);
        return tempPizza.getCost() + .50;
    }
}
