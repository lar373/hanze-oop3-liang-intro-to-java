package sample.week2.stockobserver;

import java.text.DecimalFormat;

// concrete Observer that is monitoring changes in the subject
public class AAPLObserver implements Observer {

    private int observerNr;
    private int step;

    private float price;

    private Subject stockGenerator;

    public AAPLObserver(Subject stockGenerator) {
        // (c)
        this.stockGenerator = stockGenerator;
        stockGenerator.register(this);//register observer
        this.observerNr = ObserverCounter.getNextValue();
    }

    public void update(String stock, float price) {
        // (c)
        if(stock.equals("AAPL")){
            this.price = price;
            printPrice(price);
        }
    }

    public void printPrice(float price){
        // formats decimals to two places
        DecimalFormat df = new DecimalFormat("###,###.##");
        System.out.println("AAPL = " + df.format(price));
    }
}