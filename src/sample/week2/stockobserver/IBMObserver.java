package sample.week2.stockobserver;

import java.text.DecimalFormat;

// concrete Observer that is monitoring changes in the subject
public class IBMObserver implements Observer {
    
    private int observerNr;
    private int step;

    private float price;

    private Subject stockGenerator;
    
    public IBMObserver(Subject stockGenerator) {
        // (c)
        this.stockGenerator = stockGenerator;
        stockGenerator.register(this);//register observer
        this.observerNr = ObserverCounter.getNextValue();
    }
    
    public void update(String stock, float price) {
        // (c)
        if(stock.equals("IBM")){
            this.price = price;
            printPrice(price);
        }
    }
    
    public void printPrice(float price){
        // formats decimals to two places
        DecimalFormat df = new DecimalFormat("###,###.##");
        System.out.println("IBM = " + df.format(price));
    }
}