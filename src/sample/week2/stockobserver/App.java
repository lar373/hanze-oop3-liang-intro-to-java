package sample.week2.stockobserver;

import javafx.application.Application;
import javafx.stage.Stage; 



public class App extends Application { 

    private Application MyChart;

    public App(){
        this.MyChart = new MyChart();
    }

    @Override 
    public void start(Stage stage) {
        // create the Subject
        StockGenerator stockGen = new StockGenerator();

        // create 3 runnables, pass them Subject and initial stock proces
        Runnable genIBM = new UpdateStock(stockGen, "IBM", 97.0f);
        Runnable genAAPL = new UpdateStock(stockGen, "AAPL", 174.6f);
        Runnable genGOOG = new UpdateStock(stockGen, "GOOG", 267.4f);

        // create the observer counter
//        ObserverCounter cnt = new ObserverCounter();

        // create Observers and pass Subject
        Observer ibmObserver = new IBMObserver(stockGen);
        Observer aaplObserver = new AAPLObserver(stockGen);
        Observer googObserver = new GOOGObserver(stockGen);

        // and start 3 threads        
        new Thread(genIBM).start();
        new Thread(genAAPL).start();
        new Thread(genGOOG).start();
    }

    private void updateChart(){

    }
}
