package sample.week2.stockobserver;

public interface Observer {

    // update method is called when the Subject changes
    public void update(String stock, float price);
}
