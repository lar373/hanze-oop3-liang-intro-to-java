package sample.week2.stockobserver;

import java.util.ArrayList;

public class StockGenerator implements Subject {
    
    private ArrayList<Observer> observers;
    
    public StockGenerator(){
        // (a)
        this.observers = new ArrayList<>();
    }
    
    public void register(Observer newObserver) {
        // (a)
        this.observers.add(newObserver);
    }

    public void unregister(Observer deleteObserver) {
        // (a)
        this.observers.remove(deleteObserver);
    }

    public void notifyObserver(String stock, float price) {
        // cycle through all observers and notifies them of price changes
        // (a)
        for(Observer observer :observers){
            observer.update(stock, price);
        }
    }
}
