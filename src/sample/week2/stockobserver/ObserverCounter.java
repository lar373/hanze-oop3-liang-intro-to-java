package sample.week2.stockobserver;

class ObserverCounter {
   
    private static int counter;

    ObserverCounter() {
        counter = 0;
    }

    public static int getNextValue()
    {
        return ++counter;
    }
}
