package sample.week2.stockobserver;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.stage.Stage; 
import javafx.scene.chart.LineChart; 
import javafx.scene.chart.NumberAxis; 
import javafx.scene.chart.XYChart; 
         
public class MyChart extends Application { 
    // series s is used in seperate thread (therefore must be static)
    static XYChart.Series IBMCurve;
    static XYChart.Series AAPLCurve;
    static XYChart.Series GOOGCurve;

    @Override
    public void start(Stage stage) {

        // defining the x&y axis             
        NumberAxis xAxis = new NumberAxis(0, 20, 1);
        xAxis.setLabel("Time");
        NumberAxis yAxis = new NumberAxis(0, 350, 50); 
        yAxis.setLabel("Price");
        
        // creating the line chart 
        LineChart linechart = new LineChart(xAxis, yAxis);  
        linechart.setTitle("Stock Prices");

        // defining the curves
        IBMCurve = new XYChart.Series();
        IBMCurve.setName("IBM");
        linechart.getData().add(IBMCurve);

        AAPLCurve = new XYChart.Series();
        AAPLCurve.setName("AAPL");
        linechart.getData().add(AAPLCurve);

        GOOGCurve = new XYChart.Series();
        GOOGCurve.setName("GOOG");
        linechart.getData().add(GOOGCurve);

        Group root = new Group(linechart); 
        Scene scene = new Scene(root, 600, 400);  
        stage.setScene(scene);
        stage.show();         

        Thread t = new Thread(new Test());
        t.start();
    }


    // inner class
    class Test implements Runnable {
        @Override
        public void run() {
            while(true){
                try{
                    Platform.runLater(()->MyChart.IBMCurve.getData().add(new XYChart.Data(1, 50)));
                    Thread.sleep(1000);
                    Platform.runLater(()->MyChart.AAPLCurve.getData().add(new XYChart.Data(2, 100)));
                    Thread.sleep(1000);
                    Platform.runLater(()->MyChart.GOOGCurve.getData().add(new XYChart.Data(3, 123)));
                    Thread.sleep(1000);
                }catch(Exception ex){
                   ex.printStackTrace();
                }
            }
        }
    }
}

