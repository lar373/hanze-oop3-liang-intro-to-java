package sample.week2.carfactory;

import java.util.Random;

public class Main {

    public static void main(String[] args) throws Exception {
        CarFactory factory = null;

        for (int i=0; i<20; i++){
            java.util.Random rand = new java.util.Random();
            int r1 = rand.nextInt(4) + 1;
            switch (r1) {
                // your code
                //Alle verschillende factories kunnen gereduceerd worden tot 1 factory
                //door if 'sedan' return new Sedan() etc etc. ipv aparte factory per Car
                case 1:
                    factory = new SedanFactory();
                    break;
                case 2:
                    factory = new PickUpFactory();
                    break;
                case 3:
                    factory = new StationWagonFactory();
                    break;
                case 4:
                    factory = new SUVFactory();
                    break;
            }

            Car car = factory.makeCar();
        
            // your code
            System.out.println("Type of car is " + car.toString() + " with price " + car.getCost());
        }
    }
}
