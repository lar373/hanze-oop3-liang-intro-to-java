package sample.week2.carfactory;

// abstract factory
interface CarFactory {

    Car makeCar();
}

// concrete factories
