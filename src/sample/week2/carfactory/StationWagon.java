package sample.week2.carfactory;

public class StationWagon implements Car {
    @Override
    public int getCost() {
        return 40000;
    }

    @Override
    public String getExtras() {
        return "";
    }
}
