package sample.week2.carfactory;

public class PickUp implements Car {
    @Override
    public int getCost() {
        return 50000;
    }

    @Override
    public String getExtras() {
        return "";
    }
}
