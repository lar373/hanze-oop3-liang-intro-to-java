package sample.week2.carfactory;

public class SUVFactory implements CarFactory{
    @Override
    public Car makeCar() {
        System.out.println("* Let's make an SUV *");
        return new SUV();
    }
}
