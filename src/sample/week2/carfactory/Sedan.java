package sample.week2.carfactory;

public class Sedan implements Car {

    @Override
    public int getCost() {
        return 30000;
    }

    @Override
    public String getExtras() {
        return "";
    }
}
