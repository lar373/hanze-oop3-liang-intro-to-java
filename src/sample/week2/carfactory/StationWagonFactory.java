package sample.week2.carfactory;

public class StationWagonFactory implements CarFactory {
    @Override
    public Car makeCar() {
        System.out.println("* Let's make an StationWagon *");
        return new StationWagon();
    }
}
