package sample.week2.carfactory;

public class SedanFactory implements CarFactory{

    @Override
    public Car makeCar() {
        System.out.println("* Let's make an Sedan *");
        return new Sedan();
    }
}
