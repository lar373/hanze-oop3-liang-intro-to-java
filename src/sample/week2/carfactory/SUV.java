package sample.week2.carfactory;

public class SUV implements Car {
    @Override
    public int getCost() {
        return 60000;
    }

    @Override
    public String getExtras() {
        return "";
    }
}
