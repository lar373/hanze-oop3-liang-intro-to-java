package sample.week2.carfactory;

// abstract product
interface Car {

    public int getCost();
    public String getExtras();
}

// the concrete products
