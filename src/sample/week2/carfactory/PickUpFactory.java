package sample.week2.carfactory;

public class PickUpFactory implements CarFactory {
    @Override
    public Car makeCar() {
       System.out.println("* Let's make a PickUp *");
        return new PickUp();
    }
}
